#include <iostream>

using namespace std;

class TreeItem {
private:
    int value;
    TreeItem* left;
    TreeItem* right;

    void copy(TreeItem*);
public:
    TreeItem(int val) : value(val), left(NULL), right(NULL) { }

    void addChild(TreeItem*);
    void printAsc();
    void printDesc();
    TreeItem* remove(int value);
    TreeItem* max();
    bool has(int val);

    ~TreeItem() {
        cout << "Deleting memory of Item with value = " << value << endl;
        if (left != NULL) {
            delete(left);
        }
        if (right != NULL) {
            delete(right);
        }
    }
};

void TreeItem::copy(TreeItem *item) {
    value = item->value;
    left = item->left;
    right = item->right;
}

void TreeItem::addChild(TreeItem* item) {
    if (item->value < value) {
        if (left != NULL) {
            left->addChild(item);
        } else {
            left = item;
        }
    } else {
        if (right != NULL) {
            right->addChild(item);
        } else {
            right = item;
        }
    }
}

void TreeItem::printAsc() {
    if (left != NULL) {
        left->printAsc();
    }
    cout << value << ' ';
    if (right != NULL) {
        right->printAsc();
    }
}

void TreeItem::printDesc() {
    if (right != NULL) {
        right->printDesc();
    }
    cout << value << ' ';
    if (left != NULL) {
        left->printDesc();
    }
}

TreeItem* TreeItem::max() {
    if (right != NULL) {
        return right->max();
    } else {
        return this;
    }
}

TreeItem* TreeItem::remove(int value) {
    TreeItem* toRemove = NULL;
    if (value == this->value) {
        if (left == NULL && right == NULL) { // Нет обоих детей
            toRemove = this;
        } else if (left != NULL && right == NULL) {
            toRemove = left;
            copy(toRemove);
        } else if (right != NULL && left == NULL) {
            toRemove = right;
            copy(toRemove);
        } else {
            if (left->right == NULL) {
                toRemove = left;
                this->value = toRemove->value;
                left = toRemove->left;
            } else {
                TreeItem* maxItem = left->max();
                this->value = maxItem->value;
                toRemove = left->remove(maxItem->value);
            }
        }
    } else if (value < this->value && left != NULL) {
        TreeItem* result = left->remove(value);
        if (result == left) {
            toRemove = left;
            left = NULL;
        }
    } else if (value > this->value && right != NULL) {
        TreeItem* result = right->remove(value);
        if (result == right) {
            toRemove = right;
            right = NULL;
        }
    }
    if (toRemove != NULL) {
        toRemove->left = NULL;
        toRemove->right = NULL;
    }
    return toRemove;
}

bool TreeItem::has(int val) {
    if (value == val) {
        return true;
    } else if (val < value && left != NULL) {
        return left->has(val);
    } else if (right != NULL) {
        return right->has(val);
    } else {
        return false;
    }
}

class BinaryTree {
private:
    TreeItem* root;
public:
    BinaryTree() : root(NULL) { }

    void add(int value);
    bool remove(int value);
    void print(bool desc = false);
    bool has(int value);

    ~BinaryTree() {
        delete(root);
    }

    friend ostream& operator <<(ostream& out, BinaryTree& tree);
};

void BinaryTree::add(int value) {
    TreeItem* item = new TreeItem(value);
    if (root == NULL) {
        root = item;
    } else {
        root->addChild(item);
    }
}

bool BinaryTree::remove(int value) {
    if (root != NULL) {
        TreeItem* toRemove = root->remove(value);
        if (toRemove != NULL) {
            if (root == toRemove) {
                root = NULL;
            }
            delete(toRemove);
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

void BinaryTree::print(bool desc) {
    if (root != NULL) {
        if (desc) {
            root->printDesc();
        } else {
            root->printAsc();
        }
    } else {
        cout << "Heap is empty" << endl;
    }
}

bool BinaryTree::has(int value) {
    return root != NULL && root->has(value);
}

ostream &operator<<(ostream &out, BinaryTree &tree) {
    // Вывод будет все равно в cout. При большей кастомизации код будет сложнее читать, увы.
    tree.print();
    return out;
}

// Пример использования ссылки
void deleteElement(BinaryTree& tree, int val) {
    cout << (tree.remove(val) ? "Deleted: " : "No such element: ") << val << endl;
}

// Пример использования ссылки
void checkElement(BinaryTree& tree, int val) {
    cout << (tree.has(val) ? "There is element: " : "No such element: ") << val << endl;
}

int main() {
    BinaryTree tree;

    tree.print();
    cout << endl;

    tree.add(5);
    cout << tree << endl;

    tree.add(1);
    cout << tree << endl;

    tree.add(10);
    cout << tree << endl;

    tree.add(99);
    cout << tree << endl;

    tree.print(true);
    cout << endl;

    checkElement(tree, 5);

    deleteElement(tree, 666);
    deleteElement(tree, 5);

    checkElement(tree, 5);

    cout << tree << endl;

    return 0;
}